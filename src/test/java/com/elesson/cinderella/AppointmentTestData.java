package com.elesson.cinderella;

import com.elesson.cinderella.model.Appointment;
import com.elesson.cinderella.model.Seance;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;

public class AppointmentTestData {

    public static final int ADMIN_ID = 1;
    public static final int CLIENT2_ID = 2;
    public static final int CLIENT3_ID = 3;
    public static final int CLIENT4_ID = 4;

    public static final LocalDate LOCAL_DATE_8 = LocalDate.of(2018, Month.MAY, 15);
    public static final LocalDate LOCAL_DATE_9 = LocalDate.of(2018, Month.MAY, 16);
    public static final LocalDate LOCAL_DATE_FUTURE = LocalDate.of(2028, Month.MAY, 16);

    public static final Seance SEANCE_1 = new Seance(1, LocalTime.of(8,0), LocalTime.of(8,55));
    public static final Seance SEANCE_2 = new Seance(2, LocalTime.of(9,0), LocalTime.of(9,55));
    public static final Seance SEANCE_3 = new Seance(3, LocalTime.of(10,0), LocalTime.of(10,55));
    public static final Seance SEANCE_4 = new Seance(4, LocalTime.of(11,0), LocalTime.of(11,55));
    public static final Seance SEANCE_5 = new Seance(5, LocalTime.of(12,0), LocalTime.of(12,55));


    public static final Appointment APP1 = new Appointment(1, ADMIN_ID, CLIENT2_ID, LOCAL_DATE_8, SEANCE_1);
    public static final Appointment APP2 = new Appointment(2, ADMIN_ID, CLIENT3_ID, LOCAL_DATE_8, SEANCE_3);
    public static final Appointment APP3 = new Appointment(3, ADMIN_ID, CLIENT4_ID, LOCAL_DATE_8, SEANCE_4);
    public static final Appointment APP4 = new Appointment(4, ADMIN_ID, CLIENT2_ID, LOCAL_DATE_9, SEANCE_2);
    public static final Appointment APP5 = new Appointment(5, ADMIN_ID, CLIENT4_ID, LOCAL_DATE_9, SEANCE_4);
    public static final Appointment APP6 = new Appointment(6, ADMIN_ID, CLIENT3_ID, LOCAL_DATE_9, SEANCE_1);
    public static final Appointment APP_FUTURE = new Appointment(7, ADMIN_ID, CLIENT3_ID, LOCAL_DATE_FUTURE, SEANCE_1);

    public static final Seance[] SEANCES = {SEANCE_1, SEANCE_2, SEANCE_3, SEANCE_4, SEANCE_5};

    public static final Appointment getCreated() {
        return new Appointment(null, ADMIN_ID, CLIENT2_ID, LOCAL_DATE_9, SEANCE_3);
    }

}
