package com.elesson.cinderella;

import com.elesson.cinderella.model.Role;
import com.elesson.cinderella.model.User;

public class UserTestData {

    public static final int ADMIN_ID = 1;
    public static final int USER_ID = 2;
    public static final int CLIENT_ID = 3;
    public static final int VIP_ID = 4;
    public static final int MASTER_ID = 5;
    public static final int NEXT_ID = 6;

    public static final String ADMIN_EMAIL = "admin@gmail.com";

    public static final User ADMIN = new User(ADMIN_ID, "Admin", ADMIN_EMAIL, "admin", Role.MASTER, Role.CLIENT, Role.ADMIN);
    public static final User USER = new User(USER_ID, "User", "user@gmail.com", "password", Role.CLIENT);
    public static final User CLIENT = new User(CLIENT_ID, "Client", "client@gmail.com", "password", Role.CLIENT);
    public static final User VIP = new User(VIP_ID, "VIP", "vip@gmail.com", "password", Role.CLIENT);
    public static final User MASTER = new User(MASTER_ID, "Master", "master@gmail.com", "master", Role.CLIENT, Role.MASTER);

    public static final User ADMIN_MASTER = new User(ADMIN_ID, "Admin", ADMIN_EMAIL, "admin", Role.MASTER);
    public static final User MASTER_MASTER = new User(MASTER_ID, "Master", "master@gmail.com", "master", Role.MASTER);

    public static final User getCreated() {
        return new User(null, "Created", "created@gmail.com", "password", Role.CLIENT);
    }

    public static final User getUpdated() {
        return new User(MASTER.getId(), MASTER.getName(), MASTER.getEmail(), MASTER.getPassword(),
                Role.MASTER, Role.CLIENT);
    }

}

