package com.elesson.cinderella.service;

import com.elesson.cinderella.DbUtil;
import com.elesson.cinderella.model.Appointment;
import com.elesson.cinderella.dao.exception.DuplicateAppointmentException;
import com.elesson.cinderella.service.exception.*;
import org.junit.BeforeClass;
import org.junit.Test;

import static com.elesson.cinderella.AppointmentTestData.*;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;


public class AppointmentServiceNonModTest {

    private AppointmentService aps = new AppointmentServiceImpl();

    @BeforeClass
    public static void setUp() throws Exception {
        DbUtil.setup();
    }

    @Test
    public void get() {
        assertEquals(APP1, aps.get(1));
    }

    @Test(expected = NotFoundEntityException.class)
    public void getNotFound() {
        aps.get(13);
    }

    @Test
    public void getByUserId() {
        assertArrayEquals(aps.getByUserId(CLIENT3_ID).toArray(), new Appointment[]{APP2, APP6, APP_FUTURE});
    }

    @Test
    public void getFutureByUserId() {
        assertArrayEquals(aps.getFutureByUserId(CLIENT3_ID).toArray(), new Appointment[]{APP_FUTURE});
    }

    @Test
    public void getAllByDate() {
        assertEquals(aps.getAllByDate(ADMIN_ID, LOCAL_DATE_9).size(), SEANCES.length);
    }

    @Test
    public void getByDate() {
        assertArrayEquals(aps.getByDate(ADMIN_ID, LOCAL_DATE_8).toArray(), new Appointment[]{APP1, APP2, APP3});
    }

    @Test(expected = NotFoundEntityException.class)
    public void deleteNotFound() throws Exception {
        aps.delete(3, CLIENT2_ID);
    }

    @Test(expected = DuplicateAppointmentException.class)
    public void createDuplicate() throws Exception  {
        aps.create(APP5);
    }
}
