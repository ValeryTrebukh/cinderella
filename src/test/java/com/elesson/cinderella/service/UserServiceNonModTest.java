package com.elesson.cinderella.service;

import com.elesson.cinderella.DbUtil;
import com.elesson.cinderella.model.User;
import com.elesson.cinderella.service.exception.NotFoundEntityException;
import org.junit.BeforeClass;
import org.junit.Test;


import static com.elesson.cinderella.UserTestData.*;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class UserServiceNonModTest {

    private UserService us = new UserServiceImpl();

    @BeforeClass
    public static void setUp() throws Exception {
        DbUtil.setup();
    }

    @Test
    public void getAll() {
        assertArrayEquals(us.getAll().toArray(), new User[]{ADMIN, USER, CLIENT, VIP, MASTER});
    }

    @Test
    public void getAllMasters() {
        assertArrayEquals(us.getAllMasters().toArray(), new User[]{ADMIN_MASTER, MASTER_MASTER});
    }

    @Test
    public void getById() {
        assertEquals(us.get(MASTER_ID), MASTER);
    }

    @Test
    public void getByEmail() {
        assertEquals(us.getByEmail(ADMIN_EMAIL), ADMIN);
    }

    @Test(expected = NotFoundEntityException.class)
    public void deleteNotFound() throws Exception {
        us.delete(8);
    }

    @Test(expected = NotFoundEntityException.class)
    public void getNotFound() throws Exception {
        us.get(8);
    }

    @Test(expected = NotFoundEntityException.class)
    public void getByEmailNotFound() throws Exception {
        us.getByEmail("nobody@gmail.com");
    }
}
