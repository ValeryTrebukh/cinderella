package com.elesson.cinderella.service;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ UserServiceModTest.class, UserServiceNonModTest.class })
public class UserServiceTest {

}
