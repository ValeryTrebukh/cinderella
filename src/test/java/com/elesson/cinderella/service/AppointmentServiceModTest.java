package com.elesson.cinderella.service;

import com.elesson.cinderella.DbUtil;
import com.elesson.cinderella.model.Appointment;
import org.junit.Before;
import org.junit.Test;

import static com.elesson.cinderella.AppointmentTestData.*;
import static org.junit.Assert.*;

public class AppointmentServiceModTest {

    private AppointmentService aps = new AppointmentServiceImpl();

    @Before
    public void setUp() throws Exception {
        DbUtil.setup();
    }

    @Test
    public void create() {
        Appointment created = getCreated();
        aps.create(created);
        created.setId(8);
        assertArrayEquals(aps.getByDate(ADMIN_ID, LOCAL_DATE_9).toArray(), new Appointment[]{APP6, APP4, created, APP5});
    }

    @Test
    public void delete() {
        aps.delete(4, CLIENT2_ID);
        assertArrayEquals(aps.getByDate(ADMIN_ID, LOCAL_DATE_9).toArray(), new Appointment[]{APP6, APP5});
    }
}