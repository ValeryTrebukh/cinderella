package com.elesson.cinderella.service;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;


@RunWith(Suite.class)
@SuiteClasses({ AppointmentServiceModTest.class, AppointmentServiceNonModTest.class })
public class AppointmentServiceTest {

}
