package com.elesson.cinderella.service;

import com.elesson.cinderella.DbUtil;
import com.elesson.cinderella.model.User;
import org.junit.Before;
import org.junit.Test;

import static com.elesson.cinderella.UserTestData.*;
import static org.junit.Assert.assertArrayEquals;

public class UserServiceModTest {

    private UserService us = new UserServiceImpl();

    @Before
    public void setUp() throws Exception {
        DbUtil.setup();
    }

    @Test
    public void create() {
        User created = getCreated();
        us.create(created);
        created.setId(NEXT_ID);
        assertArrayEquals(us.getAll().toArray(), new User[]{ADMIN, USER, CLIENT, VIP, MASTER, created});
    }

    @Test
    public void update() {
        User updated = getUpdated();
        updated.setName("SuperMaster");
        us.create(updated);
        assertArrayEquals(us.getAll().toArray(), new User[]{ADMIN, USER, CLIENT, VIP, updated});
    }

    @Test
    public void delete() {
        us.delete(CLIENT_ID);
        assertArrayEquals(us.getAll().toArray(), new User[]{ADMIN, USER, VIP, MASTER});
    }
}
