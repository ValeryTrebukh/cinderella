package com.elesson.cinderella;

import com.elesson.cinderella.dao.utils.DBConnection;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class DbUtil {
    public static void setup() throws SQLException {
        Connection connection = DBConnection.getConnection();
        Statement st = null;
        try {
            st = connection.createStatement();

            st.execute("DROP TABLE IF EXISTS feedback;");
            st.execute("DROP TABLE IF EXISTS appointments;");
            st.execute("DROP TABLE IF EXISTS seances;");
            st.execute("DROP TABLE IF EXISTS user_roles;");
            st.execute("DROP TABLE IF EXISTS users;");

            st.execute("CREATE TABLE users\n" +
                    "(\n" +
                    "  id        INTEGER AUTO_INCREMENT PRIMARY KEY,\n" +
                    "  name      VARCHAR (255) NOT NULL,\n" +
                    "  email     VARCHAR (255) NOT NULL,\n" +
                    "  password  VARCHAR (255) NOT NULL\n" +
                    ");");

            st.execute("CREATE TABLE user_roles\n" +
                    "(\n" +
                    "  user_id   INTEGER NOT NULL,\n" +
                    "  role      VARCHAR (255),\n" +
                    "  CONSTRAINT user_role_idx UNIQUE (user_id, role),\n" +
                    "  FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE\n" +
                    ");");

            st.execute("CREATE TABLE seances\n" +
                    "(\n" +
                    "  id          INTEGER AUTO_INCREMENT PRIMARY KEY,\n" +
                    "  start_time  TIME NOT NULL,\n" +
                    "  end_time    TIME NOT NULL\n" +
                    ");");

            st.execute("CREATE TABLE appointments\n" +
                    "(\n" +
                    "  id          INTEGER AUTO_INCREMENT PRIMARY KEY,\n" +
                    "  master_id   INTEGER,\n" +
                    "  client_id   INTEGER,\n" +
                    "  date        DATE    NOT NULL,\n" +
                    "  seance_id   INTEGER,\n" +
                    "  CONSTRAINT client_unique UNIQUE (client_id, date, seance_id),\n" +
                    "  CONSTRAINT master_unique UNIQUE (master_id, date, seance_id),\n" +
                    "  FOREIGN KEY (master_id) REFERENCES users (id) ON DELETE CASCADE,\n" +
                    "  FOREIGN KEY (client_id) REFERENCES users (id) ON DELETE CASCADE,\n" +
                    "  FOREIGN KEY (seance_id) REFERENCES seances(id)\n" +
                    ");");


            st.execute("CREATE TABLE feedback\n" +
                    "(\n" +
                    "  app_id      INTEGER UNIQUE NOT NULL,\n" +
                    "  status VARCHAR (20),\n" +
                    "  description VARCHAR (255),\n" +
                    "  FOREIGN KEY (app_id) REFERENCES appointments (id) ON DELETE CASCADE\n" +
                    ");");

            st.execute("INSERT INTO users (name, email, password) VALUES\n" +
                    "  ('Admin', 'admin@gmail.com', 'admin'),\n" +
                    "  ('User', 'user@gmail.com', 'password'),\n" +
                    "  ('Client', 'client@gmail.com', 'password'),\n" +
                    "  ('VIP', 'vip@gmail.com', 'password'),\n" +
                    "  ('Master', 'master@gmail.com', 'master');");

            st.execute("INSERT INTO user_roles (role, user_id) VALUES\n" +
                    "  ('MASTER', 1),\n" +
                    "  ('CLIENT', 2),\n" +
                    "  ('CLIENT', 3),\n" +
                    "  ('CLIENT', 4),\n" +
                    "  ('CLIENT', 1),\n" +
                    "  ('ADMIN', 1),\n" +
                    "  ('CLIENT', 5),\n" +
                    "  ('MASTER', 5);");

            st.execute("INSERT INTO seances (start_time, end_time) VALUES\n" +
                    "  ('8:00:00', '8:55:00'),\n" +
                    "  ('9:00:00', '9:55:00'),\n" +
                    "  ('10:00:00', '10:55:00'),\n" +
                    "  ('11:00:00', '11:55:00'),\n" +
                    "  ('12:00:00', '12:55:00');");

            st.execute("INSERT INTO appointments (master_id, client_id, date, seance_id) VALUES\n" +
                    "  (1, 2, '2018-05-15', 1),\n" +
                    "  (1, 3, '2018-05-15', 3),\n" +
                    "  (1, 4, '2018-05-15', 4),\n" +
                    "  (1, 2, '2018-05-16', 2),\n" +
                    "  (1, 4, '2018-05-16', 4),\n" +
                    "  (1, 3, '2018-05-16', 1),\n" +
                    "  (1, 3, '2028-05-16', 1);");
        } finally {
            DBConnection.close(connection, st, null);
        }
    }
}
