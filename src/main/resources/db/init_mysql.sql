DROP TABLE IF EXISTS feedback;
DROP TABLE IF EXISTS appointments;
DROP TABLE IF EXISTS seances;
DROP TABLE IF EXISTS user_roles;
DROP TABLE IF EXISTS users;

CREATE TABLE users
(
  id        INTEGER AUTO_INCREMENT PRIMARY KEY,
  name      VARCHAR (255) NOT NULL,
  email     VARCHAR (255) UNIQUE  NOT NULL ,
  password  VARCHAR (255) NOT NULL
);

CREATE TABLE user_roles
(
  user_id   INTEGER NOT NULL,
  role      VARCHAR (255),
  CONSTRAINT user_role_idx UNIQUE (user_id, role),
  FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE
);

CREATE TABLE seances
(
  id          INTEGER AUTO_INCREMENT PRIMARY KEY,
  start_time  TIME NOT NULL,
  end_time    TIME NOT NULL
);

CREATE TABLE appointments
(
  id          INTEGER AUTO_INCREMENT PRIMARY KEY,
  master_id   INTEGER,
  client_id   INTEGER,
  date        DATE    NOT NULL,
  seance_id   INTEGER,
  CONSTRAINT client_unique UNIQUE (client_id, date, seance_id),
  CONSTRAINT master_unique UNIQUE (master_id, date, seance_id),
  FOREIGN KEY (master_id) REFERENCES users (id) ON DELETE CASCADE,
  FOREIGN KEY (client_id) REFERENCES users (id) ON DELETE CASCADE,
  FOREIGN KEY (seance_id) REFERENCES seances(id)
);

CREATE TABLE feedback
(
  app_id      INTEGER UNIQUE NOT NULL,
  status      VARCHAR(20),
  description VARCHAR (255),
  FOREIGN KEY (app_id) REFERENCES appointments (id) ON DELETE CASCADE
);