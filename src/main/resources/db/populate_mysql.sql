DELETE FROM appointments;
DELETE FROM seances;
DELETE FROM user_roles;
DELETE FROM users;

INSERT INTO users (name, email, password) VALUES
  ('Ромашова Ольга', 'ro@gmail.com', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918'),
  ('Тищенко Екатерина', 'te@gmail.com', '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8'),
  ('Потапова Алеся', 'pa@gmail.com', '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8'),
  ('Смирнова Ольга', 'so@gmail.com', '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8'),
  ('Буланов Тимур', 'bt@gmail.com', 'fc613b4dfd6736a7bd268c8a0e74ed0d1c04a959f59dd74ef2874983fd443fc9');

# user=ro@gmail.com
# password=admin

INSERT INTO user_roles (role, user_id) VALUES
  ('MASTER', 1),
  ('CLIENT', 2),
  ('CLIENT', 3),
  ('CLIENT', 4),
  ('CLIENT', 1),
  ('ADMIN', 1),
  ('CLIENT', 5),
  ('MASTER', 5);

INSERT INTO seances (start_time, end_time) VALUES
  ('8:00:00', '8:55:00'),
  ('9:00:00', '9:55:00'),
  ('10:00:00', '10:55:00'),
  ('11:00:00', '11:55:00'),
  ('12:00:00', '12:55:00'),
  ('13:00:00', '13:55:00'),
  ('14:00:00', '14:55:00'),
  ('15:00:00', '15:55:00');

INSERT INTO appointments (master_id, client_id, date, seance_id) VALUES
  (1, 2, '2018-05-22', 1),
  (1, 3, '2018-05-22', 3),
  (1, 4, '2018-05-22', 4),
  (1, 2, '2018-05-23', 2),
  (1, 4, '2018-05-23', 4),
  (1, 3, '2019-05-23', 1);