package com.elesson.cinderella.web.servlet;

import com.elesson.cinderella.model.Appointment;
import com.elesson.cinderella.model.User;
import com.elesson.cinderella.service.AppointmentService;
import com.elesson.cinderella.service.AppointmentServiceImpl;
import com.elesson.cinderella.dao.exception.DBException;
import com.elesson.cinderella.service.util.Paginator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;


/**
 * The {@code ScheduleServlet} class purpose is to form a list of {@code Appointment} class objects.
 * Resulting collection depends on attributes provided in URL.
 * Possible URL attributes:
 *      action=my - records for authorized user will be returned
 *      action=date&date=somedate - somedate will be parsed to LocalDate and the records for specified
 *          data will be returned. It is also required that masterId to be stored in session.
 *          If masterId is null you will be redirected to the page with list of masters.
 * Redirects to error page if some exception comes from DAO layer.
 */
public class ScheduleServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String action = req.getParameter("action");
        String date = req.getParameter("date");
        LocalDate localDate = date==null? LocalDate.now():LocalDate.parse(date);
        List<Appointment> appointments;
        Integer masterId;

        HttpSession session = req.getSession();
        User authUser = (User)session.getAttribute("authUser");

        String mid = req.getParameter("mid");
        if(mid == null) {
            masterId = (Integer)session.getAttribute("masterId");
        } else {
            masterId = Integer.parseInt(mid);
            if(getMasterFromSession(session, masterId) != null) {
                session.setAttribute("masterId", masterId);
            } else {
                req.setAttribute("message", "minvalid");
                req.getRequestDispatcher("/jsp/errorPage.jsp").forward(req, resp);
            }
        }

        try {
            AppointmentService service = new AppointmentServiceImpl();
            switch (action == null ? "date" : action) {
                case "my":
                    Paginator<Appointment> paginator = new Paginator<>();
                    appointments = service.getFutureByUserId(authUser.getId());
                    int page = req.getParameter("page")!=null ? Integer.parseInt(req.getParameter("page")) : 1;
                    int pagesCount = paginator.getPageCount(appointments);
                    page = page > pagesCount ? pagesCount : page < 1 ? 1 : page;
                    req.setAttribute("apps", paginator.getPage(appointments, page));
                    req.setAttribute("page", page);
                    req.setAttribute("pagesCount", pagesCount);
                    req.getRequestDispatcher("jsp/personal.jsp").forward(req, resp);
                    break;
                case "lang":
                    resp.sendRedirect(req.getHeader("referer"));
                    session.setAttribute("language", req.getParameter("language"));
                    break;
                case "date":
                default:
                    if(masterId!=null) {
                        req.setAttribute("activeMaster", getMasterFromSession(session, masterId));
                        appointments = service.getAllByDate(masterId, localDate);
                        req.setAttribute("date", localDate);
                        req.setAttribute("apps", appointments);
                        req.getRequestDispatcher("jsp/schedule.jsp").forward(req, resp);
                    } else {
                        resp.sendRedirect("schedule?action=my");
                    }
                    break;
            }
        } catch (DBException e) {
            req.setAttribute("message", "db");
            req.getRequestDispatcher("/jsp/errorPage.jsp").forward(req, resp);
        }
    }

    private User getMasterFromSession(HttpSession session, Integer masterId) {
        List<User> list = (List<User>) session.getAttribute("masters");
        for(User u : list) {
            if (u.getId().equals(masterId)) {
                return u;
            }
        }
        return null;
    }
}