package com.elesson.cinderella.web.servlet;

import com.elesson.cinderella.model.*;
import com.elesson.cinderella.service.AppointmentService;
import com.elesson.cinderella.service.AppointmentServiceImpl;
import com.elesson.cinderella.dao.exception.*;
import com.elesson.cinderella.service.exception.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.time.LocalDate;


/**
 * The {@code ConfirmServlet} class purpose is to process the appointment and feedback records.
 * This class provides main functional of the application.
 *      Any authorized user may assign any free record to itself.
 *      Any Client or Master may cancel the record if it's ID stored into Appointment master or client field.
 *      Admin may request Client to provide feedback on granted service.
 *      Client may provide a feedback if it was requested. Only admin may see provided feedback.
 * Redirects to error page if some exception comes from DAO or service layer.
 */
public class ConfirmServlet extends HttpServlet {

    private static final Logger logger = LogManager.getLogger(ConfirmServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String action = req.getParameter("action");
        HttpSession session = req.getSession();
        User authUser = (User)session.getAttribute("authUser");
        Integer masterId = (Integer)session.getAttribute("masterId");
        Appointment app = null;
        Integer recId;

        try {
            AppointmentService service = new AppointmentServiceImpl();
            switch (action) {
                case "accept":
                    String date = req.getParameter("date");
                    Seance seance = new Seance(Integer.parseInt(req.getParameter("seance")), null, null);
                    app = new Appointment(null, masterId, authUser.getId(), LocalDate.parse(date), seance);
                    service.create(app);
                    logger.info("User id={} created new appointment id={}", authUser.getId(), app.getId());
                    break;
                case "cancel":
                    recId = Integer.parseInt(req.getParameter("id"));
                    service.delete(recId, authUser.getId());
                    logger.info("User id={} canceled appointment id={}", authUser.getId(), recId);
                    break;
                case "request":
                    recId = Integer.parseInt(req.getParameter("id"));
                    service.createFeedback(recId);
                    logger.info("User id={} requested feedback for appointment id={}", authUser.getId(), recId);
                    break;
                case "write":
                    recId = Integer.parseInt(req.getParameter("id"));
                    app = service.get(recId);
                    if(!authUser.getId().equals(app.getClientId()) || app.getFeedback().getStatus().equals(Feedback.Status.RECEIVED)) {
                        throw new NotFoundEntityException("Not allowed");
                    }
                    req.setAttribute("feedbackId", recId);
                    req.getRequestDispatcher("jsp/feedback.jsp").forward(req, resp);
                    break;
            }
            resp.sendRedirect("schedule" + getAdd(req));
        } catch (NotFoundEntityException e) {
            logger.info("User {} tried delete/get record id={}", authUser.getId(), req.getParameter("id"));
            req.setAttribute("message", "notFound");
            req.getRequestDispatcher("/jsp/errorPage.jsp").forward(req, resp);
        } catch (DuplicateAppointmentException e) {
            logger.info("User {} tried to create new record {}", authUser.getId(), app.toString());
            req.setAttribute("message", "duplicate");
            req.getRequestDispatcher("/jsp/errorPage.jsp").forward(req, resp);
        } catch (DBException e) {
            req.setAttribute("message", "db");
            req.getRequestDispatcher("/jsp/errorPage.jsp").forward(req, resp);
        }
    }

    private String getAdd(HttpServletRequest req) {
        String referer = req.getHeader("referer");
        return referer.lastIndexOf("?") != -1 ? referer.substring(referer.lastIndexOf("?")) : "";
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        Integer feedbackId = Integer.parseInt(req.getParameter("feedbackId"));
        String feedback = req.getParameter("feedback");
        try {
            AppointmentService service = new AppointmentServiceImpl();
            service.writeFeedback(new Feedback(feedback), feedbackId);
        } catch (DBException e) {
            req.setAttribute("message", "db");
            req.getRequestDispatcher("/jsp/errorPage.jsp").forward(req, resp);
        }

        resp.sendRedirect("schedule");
    }
}
