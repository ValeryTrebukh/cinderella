package com.elesson.cinderella.web.servlet;

import com.elesson.cinderella.model.Role;
import com.elesson.cinderella.model.User;
import com.elesson.cinderella.service.UserService;
import com.elesson.cinderella.service.UserServiceImpl;
import com.elesson.cinderella.web.util.UserDataValidation;
import com.elesson.cinderella.dao.exception.DBException;
import com.elesson.cinderella.dao.exception.DuplicateEntityException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


/**
 * The {@code RegistrationServlet} class purpose is to allow user to register a new account.
 * This class perform validation of entered data before storing to database.
 * Invalidates the session and creates new one. After that stores user data into session.
 * Redirects to error page if some exception comes from DAO layer.
 */
public class RegistrationServlet extends HttpServlet {
    private static final Logger logger = LogManager.getLogger(RegistrationServlet.class);

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String regName = req.getParameter("regName");
        String regEmail = req.getParameter("regEmail");
        String regPassword = req.getParameter("regPass");
        String confPassword = req.getParameter("confPass");

        if(UserDataValidation.hasError(req, regName, regEmail, regPassword, confPassword)) {
            req.getRequestDispatcher("registration.jsp").forward(req, resp);
        } else {
            try {
                UserService userService = new UserServiceImpl();
                User user = new User(regName, regEmail, regPassword, Role.CLIENT);
                userService.create(user);
                HttpSession session = req.getSession();
                session.invalidate();
                session = req.getSession();
                session.setAttribute("authUser", user);
                logger.info("{} logged in", user.getName());
                session.setAttribute("masters", userService.getAllMasters());
                resp.sendRedirect("schedule");
            } catch (DuplicateEntityException de) {
                req.setAttribute("duplicate", true);
                req.getRequestDispatcher("registration.jsp").forward(req, resp);
            } catch (DBException e) {
                req.setAttribute("message", "db");
                req.getRequestDispatcher("/jsp/errorPage.jsp").forward(req, resp);
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.sendRedirect("registration.jsp");
    }
}
