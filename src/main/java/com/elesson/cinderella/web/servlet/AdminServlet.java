package com.elesson.cinderella.web.servlet;

import com.elesson.cinderella.model.Role;
import com.elesson.cinderella.model.User;
import com.elesson.cinderella.service.UserService;
import com.elesson.cinderella.service.UserServiceImpl;
import com.elesson.cinderella.dao.exception.*;
import com.elesson.cinderella.service.exception.*;
import com.elesson.cinderella.service.util.Paginator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * The {@code AdminServlet} class provides main functional for the Admins (CRUD).
 *      - View the list of users.
 *      - Create new user.
 *      - Edit created user data.
 *      - Delete the users from database.
 * Redirects to error page if some exception comes from DAO or service layer.
 */
public class AdminServlet extends HttpServlet {
    private static final Logger logger = LogManager.getLogger(AdminServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        String action = req.getParameter("action");

        try {
            UserService service = new UserServiceImpl();
            switch (action == null ? "all" : action) {
                case "edit":
                case "create":
                    final User user = "create".equals(action) ?
                            new User() : service.get(Integer.parseInt(req.getParameter("userid")));
                    req.setAttribute("user", user);
                    req.getRequestDispatcher("jsp/admin/userForm.jsp").forward(req, resp);
                    break;
                case "delete":
                    service.delete(Integer.parseInt(req.getParameter("userid")));
                    resp.sendRedirect("users");
                    break;
                case "all":
                default:
                    Paginator<User> paginator = new Paginator<>();
                    List<User> users = service.getAll();
                    int page = req.getParameter("page")!=null ? Integer.parseInt(req.getParameter("page")) : 1;
                    int pagesCount = paginator.getPageCount(users);
                    page = page > pagesCount ? pagesCount : page < 1 ? 1 : page;
                    req.setAttribute("users", paginator.getPage(users, page));
                    req.setAttribute("page", page);
                    req.setAttribute("pagesCount", paginator.getPageCount(users));
                    req.getRequestDispatcher("jsp/admin/users.jsp").forward(req, resp);
                    break;
            }
        } catch (NotFoundEntityException e) {
            logger.info("User tried obtain/delete not existed user id={}", req.getParameter("id"));
            req.setAttribute("message", "notFound");
            req.getRequestDispatcher("/jsp/errorPage.jsp").forward(req, resp);
        } catch (DBException e) {
            req.setAttribute("message", "db");
            req.getRequestDispatcher("/jsp/errorPage.jsp").forward(req, resp);
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        String regName = req.getParameter("name");
        String regEmail = req.getParameter("email");
        String regPassword = req.getParameter("password");

        User user = new User(regName, regEmail, regPassword, Role.CLIENT);

        try {
            UserService service = new UserServiceImpl();
            if (req.getParameter("userid").isEmpty()) {
                service.create(user);
            }
            else {
                user.setId(Integer.parseInt(req.getParameter("userid")));
                user.setRoles(getRolesFromRequest(req));
                service.update(user);
            }
            resp.sendRedirect("users");
        } catch (DuplicateAppointmentException e) {
            logger.info("User tried to create new record {}", user.toString());
            req.setAttribute("duplicate", true);
            req.getRequestDispatcher("jsp/admin/userForm.jsp").forward(req, resp);
        } catch (DBException e) {
            req.setAttribute("message", "db");
            req.getRequestDispatcher("/jsp/errorPage.jsp").forward(req, resp);
        }
    }

    private Set<Role> getRolesFromRequest(HttpServletRequest req) {
        Set<Role> roles = new HashSet<>();
        if(req.getParameter("roleClient")!=null) {
            roles.add(Role.CLIENT);
        }
        if(req.getParameter("roleMaster")!=null) {
            roles.add(Role.MASTER);
        }
        if(req.getParameter("roleAdmin")!=null) {
            roles.add(Role.ADMIN);
        }
        return roles;
    }
}

