package com.elesson.cinderella.model;

import java.util.EnumSet;
import java.util.Objects;
import java.util.Set;

/**
 * The {@code User} class represents an entity of application's user.
 */
public class User {
    private Integer id;
    private String name;
    private String email;
    private String password;
    private Set<Role> roles;

    /**
     * Instantiates a new User.
     *
     * @param id       the id
     * @param name     the name
     * @param email    the email
     * @param password the password
     * @param role     the role
     * @param roles    the roles
     */
    public User(Integer id, String name, String email, String password, Role role, Role... roles) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
        this.roles = EnumSet.of(role, roles);
    }

    /**
     * Instantiates a new User.
     *
     * @param name     the name
     * @param email    the email
     * @param password the password
     * @param role     the role
     * @param roles    the roles
     */
    public User(String name, String email, String password, Role role, Role... roles) {
        this(null, name, email, password, role, roles);
    }

    /**
     * The default constructor.
     */
    public User() {
    }

    /**
     * Is new boolean.
     *
     * @return true if user has no Id assigned to it.
     */
    public boolean isNew() {
        return this.id == null;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets email.
     *
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets email.
     *
     * @param email the email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Gets password.
     *
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets password.
     *
     * @param password the password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Gets roles.
     *
     * @return the set of roles
     */
    public Set<Role> getRoles() {
        return roles;
    }

    /**
     * Sets roles.
     *
     * @param roles the roles
     */
    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    /**
     * Is admin boolean.
     *
     * @return true if user has Admin role
     */
    public boolean isAdmin() {
        return roles.contains(Role.ADMIN);
    }

    /**
     * Is master boolean.
     *
     * @return true if user has Master role
     */
    public boolean isMaster() {
        return roles.contains(Role.MASTER);
    }

    /**
     * Is client boolean.
     *
     * @return true if user has Client role
     */
    public boolean isClient() {
        return roles.contains(Role.CLIENT);
    }

    @Override
    public String toString() {
        return String.format("Entity %s (%s, '%s')", getClass().getName(), id, name);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(id, user.id) &&
                Objects.equals(name, user.name) &&
                Objects.equals(email, user.email) &&
                Objects.equals(password, user.password) &&
                Objects.equals(roles, user.roles);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name, email, password, roles);
    }
}
