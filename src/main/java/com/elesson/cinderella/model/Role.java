package com.elesson.cinderella.model;

/**
 * The {@code Role} enum represents type of user's roles.
 */
public enum Role {
    CLIENT,
    MASTER,
    ADMIN
}
