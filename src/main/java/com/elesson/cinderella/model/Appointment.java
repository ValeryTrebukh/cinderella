package com.elesson.cinderella.model;

import java.time.LocalDate;
import java.util.Objects;

/**
 * The {@code Appointment} class represents an entity of user's visit to some place.
 */
public class Appointment {
    private Integer id;
    private LocalDate date;
    private Seance seance;
    private User client;
    private User master;
    private Feedback feedback;

    private Integer masterId;
    private Integer clientId;

    /**
     * Instantiates a new Appointment.
     *
     * @param id       the id
     * @param masterId the master id
     * @param clientId the client id
     * @param date     the date
     * @param seance   the seance
     */
    public Appointment(Integer id, Integer masterId, Integer clientId, LocalDate date, Seance seance) {
        this.id = id;
        this.masterId = masterId;
        this.clientId = clientId;
        this.date = date;
        this.seance = seance;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Gets date.
     *
     * @return the date
     */
    public LocalDate getDate() {
        return date;
    }

    /**
     * Gets master id.
     *
     * @return the masterId
     */
    public Integer getMasterId() {
        return masterId;
    }

    /**
     * Gets client id.
     *
     * @return the clientId
     */
    public Integer getClientId() {
        return clientId;
    }

    /**
     * Gets seance.
     *
     * @return the seance
     */
    public Seance getSeance() {
        return seance;
    }

    /**
     * Gets client.
     *
     * @return the client
     */
    public User getClient() {
        return client;
    }

    /**
     * Sets client.
     *
     * @param client the client
     */
    public void setClient(User client) {
        this.client = client;
    }

    /**
     * Gets master.
     *
     * @return the master
     */
    public User getMaster() {
        return master;
    }

    /**
     * Sets master.
     *
     * @param master the master
     */
    public void setMaster(User master) {
        this.master = master;
    }

    /**
     * Gets feedback.
     *
     * @return the feedback
     */
    public Feedback getFeedback() {
        return feedback;
    }

    /**
     * Sets feedback.
     *
     * @param feedback the feedback
     */
    public void setFeedback(Feedback feedback) {
        this.feedback = feedback;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Appointment that = (Appointment) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(date, that.date) &&
                Objects.equals(seance, that.seance) &&
                Objects.equals(masterId, that.masterId) &&
                Objects.equals(clientId, that.clientId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, date, seance, masterId, clientId);
    }

    @Override
    public String toString() {
        return "Appointment{" +
                "id=" + id +
                ", date=" + date +
                ", seance=" + seance +
                ", masterId=" + masterId +
                ", clientId=" + clientId +
                '}';
    }
}
