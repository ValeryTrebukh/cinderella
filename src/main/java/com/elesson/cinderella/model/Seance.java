package com.elesson.cinderella.model;

import java.time.LocalTime;
import java.util.Objects;

/**
 * The {@code Seance} class represents the entity of some event
 * specified by time interval.
 */
public class Seance {
    private Integer id;
    private LocalTime start;
    private LocalTime end;

    /**
     * Instantiates a new Seance.
     *
     * @param id    the id
     * @param start the start of event
     * @param end   the end of event
     */
    public Seance(Integer id, LocalTime start, LocalTime end) {
        this.id = id;
        this.start = start;
        this.end = end;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Gets start.
     *
     * @return the start time of seance
     */
    public LocalTime getStart() {
        return start;
    }

    @Override
    public String toString() {
        return start + " : " + end;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Seance seance = (Seance) o;
        return Objects.equals(id, seance.id) &&
                Objects.equals(start, seance.start) &&
                Objects.equals(end, seance.end);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, start, end);
    }
}
