package com.elesson.cinderella.model;

/**
 * The {@code Feedback} class represents a user's feedback provided to
 * some service.
 */
public class Feedback {

    private String description;
    private Status status;

    /**
     * Constructs a new Feedback from incoming params.
     *
     * @param description the description
     * @param status      the status
     */
    public Feedback(String description, Status status) {
        this.description = description;
        this.status = status;
    }

    /**
     * Constructs a new Feedback from incoming description.
     * Sets the status to Status#RECEIVED
     *
     * @param description the description
     */
    public Feedback(String description) {
        this.description = description;
        status = Status.RECEIVED;
    }

    /**
     * Constructs a new Feedback with empty description.
     * Sets the status to Status#REQUESTED
     */
    public Feedback() {
        status = Status.REQUESTED;
    }

    /**
     * Gets description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Gets status.
     *
     * @return the status
     */
    public Status getStatus() {
        return status;
    }

    /**
     * The enum Status specifies the status of {@code Feedback} object.
     */
    public enum Status {
        REQUESTED,
        RECEIVED
    }
}
