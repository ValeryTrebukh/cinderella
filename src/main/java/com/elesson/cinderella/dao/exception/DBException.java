package com.elesson.cinderella.dao.exception;

public class DBException extends RuntimeException  {
    public DBException(String message) {
        super(message);
    }
}
