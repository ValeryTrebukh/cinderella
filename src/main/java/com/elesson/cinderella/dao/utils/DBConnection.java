package com.elesson.cinderella.dao.utils;

import java.sql.*;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import com.elesson.cinderella.dao.exception.DBException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tomcat.jdbc.pool.*;

/**
 * The {@code DBConnection} class handles the database connection pool.
 * This class is responsible for initializing of pool properties,
 * producing connections for DAO layer objects, and closing the connections.
 */
public class DBConnection {

    private static final Logger logger = LogManager.getLogger(DBConnection.class);
    private static DataSource datasource;

    static {
        ResourceBundle appRB = ResourceBundle.getBundle("cinderella");
        try {
            ResourceBundle databaseRB = ResourceBundle.getBundle(appRB.getString("database"));
            PoolProperties pool = initAndGetPoolProperties(databaseRB);
            datasource = new DataSource();
            datasource.setPoolProperties(pool);
        } catch (MissingResourceException e) {
            logger.error("DB settings file not found at {}", appRB.getString("database"));
            logger.error(e.getMessage());
            throw new DBException("DB settings not found");
        }
    }

    private static PoolProperties initAndGetPoolProperties(ResourceBundle rb) {
        PoolProperties p = new PoolProperties();
        p.setUrl(rb.getString("database.url"));
        p.setDriverClassName(rb.getString("database.driver"));
        p.setUsername(rb.getString("database.username"));
        p.setPassword(rb.getString("database.password"));

        p.setJmxEnabled(true);
        p.setTestWhileIdle(false);
        p.setTestOnBorrow(true);
        p.setValidationQuery("SELECT 1");
        p.setTestOnReturn(false);
        p.setValidationInterval(30000);
        p.setTimeBetweenEvictionRunsMillis(30000);
        p.setMaxActive(100);
        p.setInitialSize(10);
        p.setMaxWait(10000);
        p.setRemoveAbandonedTimeout(60);
        p.setMinEvictableIdleTimeMillis(30000);
        p.setMinIdle(10);
        p.setLogAbandoned(true);
        p.setRemoveAbandoned(true);
        p.setJdbcInterceptors("org.apache.tomcat.jdbc.pool.interceptor.ConnectionState;"+
                        "org.apache.tomcat.jdbc.pool.interceptor.StatementFinalizer");

        return p;
    }

    /**
     * Method is responsible for obtaining connections from pool.
     *
     * @return the connection
     * @throws DBException the general exception to cover all SQL exceptions
     */
    public static Connection getConnection() throws DBException {
        Connection con = null;
        try {
            con = datasource.getConnection();
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new DBException("Unable to obtain connection from pool");
        }
        return con;
    }

    /**
     * Method is responsible for creating of {@code PreparedStatement} object
     *
     * @param connection {@code Connection} class object
     * @param query      the query
     * @param values     the values to be set into statement
     * @return the       {@code PreparedStatement} object
     * @throws SQLException the SQL exception
     */
    public static PreparedStatement getPreparedStatement(Connection connection, String query, Object... values) throws SQLException {

        PreparedStatement pst = connection.prepareStatement(query);
        if(values != null) {
            setValues(pst, values);
        }
        return pst;
    }

    /**
     * Method is responsible for creating of {@code PreparedStatement} object.
     * After call of execute() method generated keys will be available for retrieval.
     * Used by INSERT operations.
     *
     * @param connection {@code Connection} class object
     * @param query      the query
     * @param values     the values to be set into statement
     * @return the       {@code PreparedStatement} object
     * @throws SQLException the SQL exception
     * @see DBConnection#getPreparedStatement(Connection, String, Object...)
     */
    public static PreparedStatement getPreparedStatementInsert(Connection connection, String query, Object... values) throws SQLException {

        PreparedStatement pst = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        if(values != null) {
            setValues(pst, values);
        }
        return pst;
    }

    private static void setValues(PreparedStatement pst, Object[] values) throws SQLException {
        for (int i = 0; i < values.length; i++) {
            if (values[i] instanceof Date) {
                pst.setDate(i+1, (Date) values[i]);
            }
            else if (values[i] instanceof Integer) {
                pst.setInt(i+1, (Integer) values[i]);
            }
            else if (values[i] instanceof String) {
                pst.setString(i+1, (String) values[i]);
            } else {
                logger.warn("unexpected object type");
            }
        }
    }

    /**
     * Method is responsible for closing resources.
     */
    public static void close(Connection connection, Statement st, ResultSet rs) {
        try {
            if(rs!=null) rs.close();
            if(st!=null) st.close();
            connection.close();
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
    }
}
