package com.elesson.cinderella.dao;

import com.elesson.cinderella.dao.utils.DBConnection;
import com.elesson.cinderella.model.Role;
import com.elesson.cinderella.model.User;
import com.elesson.cinderella.dao.exception.DBException;
import com.elesson.cinderella.dao.exception.DuplicateEntityException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * This class provides implementation of all {@code UserDao} interface methods.
 */
public class UserDaoImpl implements UserDao {
    private static final Logger logger = LogManager.getLogger(UserDaoImpl.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public User save(User user) {
        Connection connection = DBConnection.getConnection();
        PreparedStatement pst = null;
        ResultSet rs = null;

        try {
            if(user.isNew()) {
                String query = "INSERT INTO users (name, email, password) VALUES (?, ?, ?)";
                pst = DBConnection.getPreparedStatementInsert(connection, query, user.getName(), user.getEmail(), user.getPassword());
                if(pst.executeUpdate()==1) {
                    rs = pst.getGeneratedKeys();
                    rs.next();
                    user.setId(rs.getInt(1));

                    query = "DELETE FROM user_roles WHERE user_id=?";
                    pst = DBConnection.getPreparedStatement(connection, query, user.getId());
                    pst.execute();

                    for(Role role : user.getRoles()) {
                        query = "INSERT INTO user_roles (user_id, role) VALUES (?, ?)";
                        pst = DBConnection.getPreparedStatement(connection, query, user.getId(), role.toString());
                        pst.execute();
                    }
                }
                logger.info("New user created with id={}", user.getId());
            } else {
                if(user.getPassword().isEmpty()) {
                    String query = "UPDATE users SET name=?, email=? WHERE id=?";
                    pst = DBConnection.getPreparedStatement(connection, query, user.getName(), user.getEmail(), user.getId());
                } else {
                    String query = "UPDATE users SET name=?, email=?, password=? WHERE id=?";
                    pst = DBConnection.getPreparedStatement(connection, query, user.getName(), user.getEmail(), user.getPassword(), user.getId());
                }

                if(pst.executeUpdate()!=1) {
                    return null;
                }
                else {
                    String query = "DELETE FROM user_roles WHERE user_id=?";
                    pst = DBConnection.getPreparedStatement(connection, query, user.getId());
                    pst.execute();

                    for(Role role : user.getRoles()) {
                        query = "INSERT INTO user_roles (user_id, role) VALUES (?, ?)";
                        pst = DBConnection.getPreparedStatement(connection, query, user.getId(), role.toString());
                        pst.execute();
                    }
                }
                logger.info("User data successfully updated for userId=" + user.getId());
            }
        } catch (SQLException e) {
            if(e.getMessage().contains("Duplicate")) {
                logger.error(e.getMessage());
                throw new DuplicateEntityException();
            }
            logger.error(e.getMessage());
            throw new DBException("Unable to save new record");
        }
        finally {
            DBConnection.close(connection, pst, rs);
        }
        return user;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean delete(int id) {
        Connection connection = DBConnection.getConnection();
        PreparedStatement pst = null;
        ResultSet rs = null;
        int resultRows = 0;

        try {
            String query = "DELETE FROM users WHERE id=?;";
            pst = DBConnection.getPreparedStatement(connection, query, id);
            resultRows = pst.executeUpdate();
            logger.info("Deleted record id={}", id);
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new DBException("Unable to delete record");
        }
        finally {
            DBConnection.close(connection, pst, rs);
        }

        return resultRows==1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public User getById(int id) {
        String query = "SELECT users.*, r.role FROM users JOIN user_roles r on users.id = r.user_id WHERE users.id=?";

        return getUser(query, id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public User getByEmail(String email) {
        String query = "SELECT users.*, r.role FROM users INNER JOIN user_roles r on users.id = r.user_id WHERE users.email=?";

        return getUser(query, email);
    }

    /*
    * Method returns a single user specified by it's id or email
    */
    private User getUser(String query, Object... values) {
        Connection connection = DBConnection.getConnection();
        PreparedStatement pst = null;
        ResultSet rs = null;
        User user = null;

        try {
            pst = DBConnection.getPreparedStatement(connection, query, values);
            rs = pst.executeQuery();

            if(rs.next()) {
                user = new User(rs.getInt("id"), rs.getString("name"), rs.getString("email"), rs.getString("password"),
                        Role.valueOf(rs.getString("r.role")));
                while (rs.next()) {
                    user.getRoles().add(Role.valueOf(rs.getString("r.role")));
                }
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new DBException("Unable to obtain record");
        }
        finally {
            DBConnection.close(connection, pst, rs);
        }

        return user;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<User> getAll() {

        List<User> allUsers = new ArrayList<>();
        Connection connection = DBConnection.getConnection();
        Statement st = null;
        ResultSet rs = null;
        User user = null;

        try {
            st = connection.createStatement();
            rs = st.executeQuery("SELECT users.*, r.role FROM users JOIN user_roles r on users.id = r.user_id ORDER BY user_id");

            if(rs.next()) {
                user = new User(rs.getInt("id"), rs.getString("name"), rs.getString("email"), rs.getString("password"),
                        Role.valueOf(rs.getString("r.role")));
                allUsers.add(user);
            }
            while (rs.next()) {
                if(user.getId() == rs.getInt("id")) {
                    user.getRoles().add(Role.valueOf(rs.getString("r.role")));
                }
                else {
                    user = new User(rs.getInt("id"), rs.getString("name"), rs.getString("email"), rs.getString("password"),
                            Role.valueOf(rs.getString("r.role")));
                    allUsers.add(user);
                }
            }
            logger.debug("{} records returned on request", allUsers.size());
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new DBException("Unable to obtain records");
        }
        finally {
            DBConnection.close(connection, st, rs);
        }
        return allUsers;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<User> getAllMasters() {
        List<User> allMasters = new ArrayList<>();
        Connection connection = DBConnection.getConnection();
        Statement st = null;
        ResultSet rs = null;

        try {
            st = connection.createStatement();
            rs = st.executeQuery("SELECT users.*, r.role FROM users JOIN user_roles r on users.id = r.user_id " +
                    "WHERE r.role='MASTER' ORDER BY users.id");

            while(rs.next()) {
                allMasters.add(new User(rs.getInt("id"), rs.getString("name"), rs.getString("email"),
                        rs.getString("password"), Role.valueOf(rs.getString("r.role"))));
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new DBException("Unable to obtain records");
        }
        finally {
            DBConnection.close(connection, st, rs);
        }
        return allMasters;
    }
}
