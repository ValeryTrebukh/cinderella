package com.elesson.cinderella.dao;

import com.elesson.cinderella.model.User;
import com.elesson.cinderella.dao.exception.DBException;
import com.elesson.cinderella.dao.exception.DuplicateEntityException;

import java.util.List;


/**
 * The {@code UserDao} interface provides the methods for interaction with database
 * to process the objects of {@code User} class.
 */
public interface UserDao {

    /**
     * Inserts new record to database.
     * Suppress all SQL exceptions and throws own general one.
     *
     * @param user the {@code User} class object to be stored in database
     * @return the {@code User} class object stored in database
     * @throws DuplicateEntityException in case of non-unique values (database restrictions).
     * @throws DBException              in all other cases.
     */
    User save(User user) throws DuplicateEntityException, DBException;

    /**
     * Deletes single record from database.
     * Suppress all SQL exceptions and throws own general one.
     *
     * @param id the record unique identifier.
     * @return true if a single record removed.
     * @throws DBException the general exception to cover all SQL exceptions
     */
    boolean delete(int id) throws DBException;

    /**
     * Returns an object of {@code User} class.
     * Suppress all SQL exceptions and throws own general one.
     *
     * @param id the unique identifier
     * @return the instance of {@code User} class
     * @throws DBException the general exception to cover all SQL exceptions
     */
    User getById(int id) throws DBException;

    /**
     * Returns an object of {@code User} class objects.
     * Suppress all SQL exceptions and throws own general one.
     *
     * @param email the unique identifier
     * @return the instance of {@code User} class
     * @throws DBException the general exception to cover all SQL exceptions
     */
    User getByEmail(String email) throws DBException;

    /**
     * Returns a list of {@code User} class objects.
     * Suppress all SQL exceptions and throws own general one.
     *
     * @return the list of {@code User} class objects
     * @throws DBException the general exception to cover all SQL exceptions
     */
    List<User> getAll() throws DBException;

    /**
     * Returns a list of {@code User} class objects that have a role of MASTER.
     * Suppress all SQL exceptions and throws own general one.
     *
     * @return the list of {@code User} class objects
     * @throws DBException the general exception to cover all SQL exceptions
     */
    List<User> getAllMasters() throws DBException;
}
