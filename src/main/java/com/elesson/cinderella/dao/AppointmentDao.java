package com.elesson.cinderella.dao;

import com.elesson.cinderella.model.Appointment;
import com.elesson.cinderella.model.Feedback;
import com.elesson.cinderella.dao.exception.*;

import java.time.LocalDate;
import java.util.List;

/**
 * The {@code AppointmentDao} interface provides the methods for interaction with database
 * to process the objects of {@code Appointment} class.
 */
public interface AppointmentDao {


    /**
     * Inserts new record to database.
     * Suppress all SQL exceptions and throws own general one.
     *
     * @param app the {@code Appointment} class object to be stored in database
     * @return the {@code Appointment} class object stored in database
     * @throws DuplicateAppointmentException in case of non-unique values (database restrictions).
     * @throws DBException              in all other cases.
     */
    Appointment save(Appointment app) throws DuplicateAppointmentException, DBException;

    /**
     * Deletes single record from database.
     * Suppress all SQL exceptions and throws own general one.
     *
     * @param id the record unique identifier.
     * @param userId the user id who owns this record.
     * @return true if a single record removed.
     * @throws DBException the general exception to cover all SQL exceptions
     */
    boolean delete(int id, int userId) throws DBException;


    /**
     * Returns an object of {@code Appointment} class.
     * Suppress all SQL exceptions and throws own general one.
     *
     * @param id the unique identifier
     * @return the instance of {@code Appointment} class
     * @throws DBException the general exception to cover all SQL exceptions
     */
    Appointment get(int id) throws DBException;

    /**
     * Returns a list of {@code Appointment} class objects assigned to each {@code Seance} class object.
     * In case of no {@code Appointment} object corresponds to {@code Seance} object
     * creates new {@code Appointment} instance with empty fields.
     * Always return constant size list.
     *
     * @param masterId the unique identifier of {@code User} object who owns the record
     * @param date the date for which the data requested
     * @return the list of {@code Appointment} class objects
     * @throws DBException the general exception to cover all SQL exceptions
     * @see AppointmentDao#getByDate(Integer, LocalDate)
     */
    List<Appointment> getWithEmptyByDate(Integer masterId, LocalDate date) throws DBException;

    /**
     * Returns a list of {@code Appointment} class objects assigned to each {@code Seance} class object.
     * Doesn't create empty objects.
     *
     * @param masterId the unique identifier of {@code User} object who owns the record
     * @param date the date for which the data requested
     * @return the list of {@code Appointment} class objects
     * @throws DBException the general exception to cover all SQL exceptions
     * @see AppointmentDao#getWithEmptyByDate(Integer, LocalDate)
     */
    List<Appointment> getByDate(Integer masterId, LocalDate date) throws DBException;

    /**
     * Returns a list of {@code Appointment} class objects assigned to specified user.
     *
     * @param userId the unique identifier of {@code User} object who owns the record
     * @return the list of {@code Appointment} class objects
     * @throws DBException the general exception to cover all SQL exceptions
     */
    List<Appointment> getAllByUserId(int userId) throws DBException;

    /**
     * Returns a filtered list of {@code Appointment} class objects assigned to specified user.
     * Contains only records active for the current date and later.
     *
     * @param userId the unique identifier of {@code User} object who owns the record
     * @return the list of {@code Appointment} class objects
     * @throws DBException the general exception to cover all SQL exceptions
     */
    List<Appointment> getFutureByUserId(int userId) throws DBException;

    /**
     * Inserts a new single record to database.
     * Suppress all SQL exceptions and throws own general one.
     *
     * @param id the unique identifier of {@code Appointment} objects for which it has been assigned
     * @return true if record inserted
     * @throws DuplicateEntityException in case of non-unique values (database restrictions).
     * @throws DBException              in all other cases.
     */
    boolean createFeedback(int id) throws DBException, DuplicateEntityException;

    /**
     * Updates the record in database by setting the text to description.
     *
     * @param feedback the feedback which contains the description
     * @param id       the unique identifier
     * @return         true if object successfully updated
     * @throws DBException the general exception to cover all SQL exceptions
     */
    boolean writeFeedback(Feedback feedback, int id) throws DBException;
}
