package com.elesson.cinderella.dao;

import com.elesson.cinderella.dao.utils.DBConnection;
import com.elesson.cinderella.model.Appointment;
import com.elesson.cinderella.model.Feedback;
import com.elesson.cinderella.model.Seance;
import com.elesson.cinderella.dao.exception.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * This class provides implementation of all {@code AppointmentDao} interface methods.
 */
public class AppointmentDaoImpl implements AppointmentDao {
    private static final Logger logger = LogManager.getLogger(AppointmentDaoImpl.class);

    private static final String INSERT = "INSERT INTO appointments (master_id, client_id, date, seance_id) VALUES (?, ?, ?, ?)";
    private static final String DELETE = "DELETE FROM appointments WHERE id=? AND (master_id=? OR client_id=?);";
    private static final String SELECT = "SELECT appointments.*, seances.*, feedback.* FROM appointments " +
                                            "INNER JOIN seances ON appointments.seance_id = seances.id " +
                                            "LEFT JOIN feedback ON appointments.id = feedback.app_id " +
                                            "WHERE appointments.id=?;";
    private static final String SELECT_ALL = "SELECT appointments.*, seances.*, feedback.* FROM appointments " +
                    "INNER JOIN seances ON appointments.seance_id = seances.id " +
                    "LEFT JOIN feedback ON appointments.id = feedback.app_id " +
                    "WHERE appointments.master_id=? OR appointments.client_id=? " +
                    "ORDER BY appointments.date, seances.id;";
    private static final String SELECT_FUTURE = "SELECT appointments.*, seances.*, feedback.* FROM appointments " +
                    "INNER JOIN seances ON appointments.seance_id = seances.id " +
                    "LEFT JOIN feedback ON appointments.id = feedback.app_id " +
                    "WHERE (appointments.master_id=? OR appointments.client_id=?) AND appointments.date >=?" +
                    "ORDER BY appointments.date, seances.id;";
    private static final String SELECT_BY_DATE = "SELECT appointments.*, seances.*, feedback.* FROM appointments " +
                    "INNER JOIN seances ON appointments.seance_id = seances.id " +
                    "LEFT JOIN feedback ON appointments.id = feedback.app_id " +
                    "WHERE appointments.master_id=? AND appointments.date=? ORDER BY seances.id;";
    private static final String SELECT_BY_DATE_WITH_EMPTY = "SELECT seances.*, tapp.id, tapp.master_id, tapp.client_id, feedback.* FROM seances " +
                    "LEFT JOIN (SELECT * FROM appointments WHERE appointments.master_id=? AND appointments.date=?) AS tapp " +
                    "ON seances.id=tapp.seance_id LEFT JOIN feedback ON tapp.id = feedback.app_id ORDER BY seances.id ASC;";
    private static final String UPDATE = "UPDATE feedback SET description=?, status=? WHERE app_id=?";
    private static final String INSERT_FEEDBACK = "INSERT INTO feedback (app_id, status) VALUES (?, ?)";

    /**
     * {@inheritDoc}
     */
    @Override
    public Appointment save(Appointment app) {
        Connection connection = DBConnection.getConnection();
        PreparedStatement pst = null;
        ResultSet rs = null;

        try {
            pst = DBConnection.getPreparedStatementInsert(connection, INSERT, app.getMasterId(), app.getClientId(), Date.valueOf(app.getDate()), app.getSeance().getId());
            if(pst.executeUpdate() == 1) {
                rs = pst.getGeneratedKeys();
                rs.next();
                app.setId(rs.getInt(1));
                logger.info("New record inserted with id=" + rs.getInt(1));
            }
        }
        catch (SQLException e) {
            if(e.getMessage().contains("Duplicate")) {
                logger.error(e.getMessage());
                throw new DuplicateAppointmentException();
            }
            logger.error(e.getMessage());
            throw new DBException("Unable to save new record");
        }
        finally {
            DBConnection.close(connection, pst, rs);
        }

        return app;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean delete(int id, int userId) {
        Connection connection = DBConnection.getConnection();
        PreparedStatement pst = null;
        ResultSet rs = null;
        int resultRows = 0;

        try {
            pst = DBConnection.getPreparedStatement(connection, DELETE, id, userId, userId);
            resultRows = pst.executeUpdate();
            logger.info("User id={} deleted record id={}", userId, id);
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new DBException("Unable to delete record id=" + id);
        }
        finally {
            DBConnection.close(connection, pst, rs);
        }

        return resultRows==1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Appointment get(int id) {
        Connection connection = DBConnection.getConnection();
        PreparedStatement pst = null;
        ResultSet rs = null;
        Appointment appointment = null;

        try {
            pst = DBConnection.getPreparedStatement(connection, SELECT, id);
            rs = pst.executeQuery();

            if(rs.next()) {
                Seance seance = new Seance(rs.getInt("seances.id"),
                        rs.getTime("seances.start_time").toLocalTime(),
                        rs.getTime("seances.end_time").toLocalTime());
                appointment = new Appointment(rs.getInt("appointments.id"),
                        rs.getInt("appointments.master_id"),
                        rs.getInt("appointments.client_id"),
                        rs.getDate("appointments.date").toLocalDate(), seance);
                if(rs.getString("feedback.status")!=null) {
                    appointment.setFeedback(new Feedback(rs.getString("feedback.description"),
                            Feedback.Status.valueOf(rs.getString("feedback.status"))));
                }
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new DBException("Unable to find record id=" + id);
        }
        finally {
            DBConnection.close(connection, pst, rs);
        }

        return appointment;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Appointment> getAllByUserId(int userId) {
        List<Appointment> appointments = null;
        Connection connection = DBConnection.getConnection();
        PreparedStatement pst = null;
        ResultSet rs = null;

        try {
            pst = DBConnection.getPreparedStatement(connection, SELECT_ALL, userId, userId);
            rs = pst.executeQuery();

            appointments = parseResultSet(rs);
            logger.debug("{} records returned on request", appointments.size());
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new DBException("Unable to get records for userId=" + userId);
        }
        finally {
            DBConnection.close(connection, pst, rs);
        }
        return appointments;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Appointment> getFutureByUserId(int userId) {
        List<Appointment> appointments = null;
        Connection connection = DBConnection.getConnection();
        PreparedStatement pst = null;
        ResultSet rs = null;

        try {
            pst = DBConnection.getPreparedStatement(connection, SELECT_FUTURE, userId, userId, Date.valueOf(LocalDate.now()));
            rs = pst.executeQuery();

            appointments = parseResultSet(rs);
            logger.debug("{} records returned on request", appointments.size());
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new DBException("Unable to get records for userId=" + userId);
        }
        finally {
            DBConnection.close(connection, pst, rs);
        }
        return appointments;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Appointment> getByDate(Integer masterId, LocalDate date) {
        List<Appointment> appointments = null;
        Connection connection = DBConnection.getConnection();
        PreparedStatement pst = null;
        ResultSet rs = null;

        try {;
            pst = DBConnection.getPreparedStatement(connection, SELECT_BY_DATE, masterId, Date.valueOf(date));
            rs = pst.executeQuery();
            appointments = parseResultSet(rs);
            logger.debug("{} records returned on request", appointments.size());
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new DBException("Unable to get records for masterId=" + masterId);
        }
        finally {
            DBConnection.close(connection, pst, rs);
        }
        return appointments;
    }

    /*
    * Process ResultSet to compile the list of Appointments
    */
    private List<Appointment> parseResultSet(ResultSet rs) throws SQLException {
        List<Appointment> appointments = new ArrayList<>();
        while(rs.next()) {
            Seance seance = new Seance(rs.getInt("seances.id"),
                    rs.getTime("seances.start_time").toLocalTime(),
                    rs.getTime("seances.end_time").toLocalTime());
            Appointment app = new Appointment(rs.getInt("appointments.id"),
                    rs.getInt("appointments.master_id"),
                    rs.getInt("appointments.client_id"),
                    rs.getDate("appointments.date").toLocalDate(), seance);
            if(rs.getString("feedback.status")!=null) {
                app.setFeedback(new Feedback(rs.getString("feedback.description"),
                        Feedback.Status.valueOf(rs.getString("feedback.status"))));
            }
            appointments.add(app);
        }
        return appointments;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Appointment> getWithEmptyByDate(Integer masterId, LocalDate date) {

        List<Appointment> appointments = new ArrayList<>();
        Connection connection = DBConnection.getConnection();
        PreparedStatement pst = null;
        ResultSet rs = null;

        try {
            pst = DBConnection.getPreparedStatement(connection, SELECT_BY_DATE_WITH_EMPTY, masterId, Date.valueOf(date));
            rs = pst.executeQuery();
            while(rs.next()) {
                Seance seance = new Seance(rs.getInt("seances.id"),
                                      rs.getTime("seances.start_time").toLocalTime(),
                                      rs.getTime("seances.end_time").toLocalTime());
                Appointment app = new Appointment(rs.getInt("tapp.id"),
                        rs.getInt("tapp.master_id"),
                        rs.getInt("tapp.client_id"), date, seance);
                if(rs.getString("feedback.status")!=null) {
                    app.setFeedback(new Feedback(rs.getString("feedback.description"),
                            Feedback.Status.valueOf(rs.getString("feedback.status"))));
                }
                appointments.add(app);
            }
            logger.debug("{} records returned on request", appointments.size());
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new DBException("Unable to get records for masterId=" + masterId);
        }
        finally {
            DBConnection.close(connection, pst, rs);
        }
        return appointments;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean writeFeedback(Feedback feedback, int id) {
        Connection connection = DBConnection.getConnection();
        PreparedStatement pst = null;
        ResultSet rs = null;

        try {
            pst = DBConnection.getPreparedStatementInsert(connection, UPDATE, feedback.getDescription(),
                    feedback.getStatus().toString(), id);

            return pst.executeUpdate()==1;
        }catch (SQLException e) {
            logger.error(e.getMessage());
            throw new DBException("Unable to update record");
        }
        finally {
            DBConnection.close(connection, pst, rs);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean createFeedback(int id) {
        Connection connection = DBConnection.getConnection();
        PreparedStatement pst = null;
        ResultSet rs = null;

        try {
            pst = DBConnection.getPreparedStatementInsert(connection, INSERT_FEEDBACK, id, Feedback.Status.REQUESTED.toString());
            if(pst.executeUpdate() == 1) {
                logger.info("Feedback requested for appointment id={}", id);
                return true;
            }
            return false;
        }catch (SQLException e) {
            if(e.getMessage().contains("Duplicate")) {
                logger.error(e.getMessage());
                throw new DuplicateEntityException();
            }
            logger.error(e.getMessage());
            throw new DBException("Unable to save new record");
        }
        finally {
            DBConnection.close(connection, pst, rs);
        }
    }
}
