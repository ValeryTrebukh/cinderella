package com.elesson.cinderella.service;

import com.elesson.cinderella.dao.UserDao;
import com.elesson.cinderella.dao.UserDaoImpl;
import com.elesson.cinderella.model.User;

import java.util.List;

import static com.elesson.cinderella.service.util.Security.encrypt;
import static com.elesson.cinderella.service.util.ServiceValidation.*;

/**
 * This class provides implementation of all {@code UserService} interface methods.
 */
public class UserServiceImpl implements UserService {

    /**
     * {@inheritDoc}
     */
    @Override
    public User create(User user) {
        checkNotFound(user, "user must not be null");
        user.setPassword(encrypt(user.getPassword()));
        UserDao userDao = new UserDaoImpl();
        return checkNotFound(userDao.save(user), "user must not be null");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void delete(int id) {
        UserDao userDao = new UserDaoImpl();
        checkNotFoundWithId(userDao.delete(id), id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public User get(int id) {
        UserDao userDao = new UserDaoImpl();
        return checkNotFoundWithId(userDao.getById(id), id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public User getByEmail(String email) {
        checkNotFound(email, "email must not be null");
        UserDao userDao = new UserDaoImpl();
        return checkNotFound(userDao.getByEmail(email), "email=" + email);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void update(User user) {
        checkNotFound(user, "user must not be null");
        if(!user.getPassword().isEmpty()) {
            user.setPassword(encrypt(user.getPassword()));
        }
        UserDao userDao = new UserDaoImpl();
        checkNotFoundWithId(userDao.save(user), user.getId());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<User> getAll() {
        UserDao userDao = new UserDaoImpl();
        return userDao.getAll();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<User> getAllMasters() {
        UserDao userDao = new UserDaoImpl();
        return userDao.getAllMasters();
    }

}
