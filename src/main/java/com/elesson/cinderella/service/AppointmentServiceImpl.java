package com.elesson.cinderella.service;

import com.elesson.cinderella.dao.*;
import com.elesson.cinderella.model.*;
import com.elesson.cinderella.service.util.MailSender;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.mail.MessagingException;
import java.time.LocalDate;
import java.util.*;

import static com.elesson.cinderella.service.util.ServiceValidation.checkNotFound;
import static com.elesson.cinderella.service.util.ServiceValidation.checkNotFoundWithId;

/**
 * This class provides implementation of all {@code AppointmentService} interface methods.
 */
public class AppointmentServiceImpl implements AppointmentService {
    private static final Logger logger = LogManager.getLogger(AppointmentServiceImpl.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public Appointment create(Appointment appointment) {
        checkNotFound(appointment, "appointment must not be null");
        AppointmentDao appointmentDao = new AppointmentDaoImpl();
        return appointmentDao.save(appointment);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void delete(int id, int userId) {
        AppointmentDao appointmentDao = new AppointmentDaoImpl();
        checkNotFoundWithId(appointmentDao.delete(id, userId), id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Appointment get(int id) {
        AppointmentDao appointmentDao = new AppointmentDaoImpl();
        return checkNotFoundWithId(appointmentDao.get(id), id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Appointment> getByUserId(int userId) {
        AppointmentDao appointmentDao = new AppointmentDaoImpl();
        return setClients(appointmentDao.getAllByUserId(userId));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Appointment> getFutureByUserId(int userId) {
        AppointmentDao appointmentDao = new AppointmentDaoImpl();
        return setMasters(setClients(appointmentDao.getFutureByUserId(userId)));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Appointment> getByDate(Integer masterId, LocalDate date) {
        AppointmentDao appointmentDao = new AppointmentDaoImpl();
        return setClients(appointmentDao.getByDate(masterId, date));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Appointment> getAllByDate(Integer masterId, LocalDate date) {
        AppointmentDao appointmentDao = new AppointmentDaoImpl();
        return setClients(appointmentDao.getWithEmptyByDate(masterId, date));
    }

    /*
    * Requests users by it's id from UserDao and sets objects to client field of Appointment class
    */
    private List<Appointment> setClients(List<Appointment> apps) {
        Map<Integer, User> users = new HashMap<>();
        UserDao userDao = new UserDaoImpl();
        for(Appointment app : apps) {
            Integer id = app.getClientId();
            if(!users.containsKey(id)) {
                User user = userDao.getById(id);
                users.put(id, user);
            }
            app.setClient(users.get(id));
        }
        return apps;
    }

    /*
     * Requests users by it's id from UserDao and sets objects to master field of Appointment class
     */
    private List<Appointment> setMasters(List<Appointment> apps) {
        Map<Integer, User> users = new HashMap<>();
        UserDao userDao = new UserDaoImpl();
        for(Appointment app : apps) {
            Integer id = app.getMasterId();
            if(!users.containsKey(id)) {
                User user = userDao.getById(id);
                users.put(id, user);
            }
            app.setMaster(users.get(id));
        }
        return apps;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void writeFeedback(Feedback feedback, int id) {
        AppointmentDao appointmentDao = new AppointmentDaoImpl();
        checkNotFoundWithId(appointmentDao.writeFeedback(feedback, id), id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void createFeedback(int id) {
        AppointmentDao appointmentDao = new AppointmentDaoImpl();
        Appointment app = get(id);
        UserDao userDao = new UserDaoImpl();
        sentNotificationToClient(userDao.getById(app.getClientId()), app);
        appointmentDao.createFeedback(id);
    }

    /*
     * This methods uses util class method to sent a notification to specified user.
     */
    private void sentNotificationToClient(User client, Appointment app) {
        try {
            MailSender.send(client.getEmail(), app.getDate(), app.getSeance().getStart());
            logger.debug("Notification sent");
        } catch (MessagingException e) {
            logger.error(e.getMessage());
        }
    }
}
