package com.elesson.cinderella.service;

import com.elesson.cinderella.model.Appointment;
import com.elesson.cinderella.model.Feedback;
import com.elesson.cinderella.dao.exception.*;
import com.elesson.cinderella.service.exception.*;

import java.time.LocalDate;
import java.util.List;

/**
 * The interface {@code AppointmentService} contains methods to process requests from controller to DAO layer.
 */
public interface AppointmentService {

    /**
     * Method confirms that object is not null and process it to DAO layer.
     *
     * @param appointment the {@code Appointment} class object
     * @return the {@code Appointment} class object
     * @throws DuplicateAppointmentException in case of non-unique values (database restrictions).
     * @throws DBException in all other cases.n
     */
    Appointment create(Appointment appointment) throws DuplicateAppointmentException, DBException;

    /**
     * Method process request to DAO layer and confirms the operation success.
     * If requested record does not belongs to specified user, throws an exception
     *
     * @param id the record unique identifier.
     * @param userId the unique identifier of user who owns this record.
     * @throws DBException the general exception to cover all SQL exceptions
     * @throws NotFoundEntityException if no record found for the specified combination of params
     */
    void delete(int id, int userId) throws NotFoundEntityException, DBException;

    /**
     * Method process request to DAO layer and confirms the operation success.
     *
     * @param id the unique identifier
     * @return the instance of {@code Appointment} class
     * @throws DBException the general exception to cover all SQL exceptions
     * @throws NotFoundEntityException if no record found
     */
    Appointment get(int id) throws NotFoundEntityException, DBException;

    /**
     * Method process request to DAO layer and initializes {@code User} client field.
     *
     * @param userId the unique identifier of {@code User} object who owns the records
     * @return the list of {@code Appointment} class objects
     * @throws DBException the general exception to cover all SQL exceptions
     */
    List<Appointment> getByUserId(int userId) throws DBException;

    /**
     * Method process request to DAO layer and initializes {@code User} client and master fields.
     * Returns only records actual for the current date and later.
     *
     * @param userId the unique identifier of {@code User} object who owns the record
     * @return the list of {@code Appointment} class objects
     * @throws DBException the general exception to cover all SQL exceptions
     */
    List<Appointment> getFutureByUserId(int userId) throws DBException;

    /**
     * Method process request to DAO layer and initializes {@code User} client field.
     *
     * @param masterId the unique identifier of {@code User} object who owns the record
     * @param date the date for which the data requested
     * @return the list of {@code Appointment} class objects
     * @throws DBException the general exception to cover all SQL exceptions
     */
    List<Appointment> getByDate(Integer masterId, LocalDate date) throws DBException;

    /**
     * Returns a list of {@code Appointment} class objects assigned to each {@code Seance} class object.
     * In case of no {@code Appointment} object corresponds to {@code Seance} object
     * creates new {@code Appointment} instance with empty fields.
     * Always return constant size list.
     *
     * @param masterId the unique identifier of {@code User} object who owns the record
     * @param date the date for which the data requested
     * @return the list of {@code Appointment} class objects
     * @throws DBException the general exception to cover all SQL exceptions
     */
    List<Appointment> getAllByDate(Integer masterId, LocalDate date) throws DBException;

    /**
     * Updates the description.
     *
     * @param feedback the feedback which contains the description
     * @param id       the unique identifier
     * @throws DBException the general exception to cover all SQL exceptions
     * @throws NotFoundEntityException if no record found with such id
     */
    void writeFeedback(Feedback feedback, int id) throws DBException, NotFoundEntityException;

    /**
     * Method process request to DAO layer and confirms the operation success.
     *
     * @param id the unique identifier of {@code Appointment} objects for which it has been assigned
     * @throws DuplicateEntityException in case of non-unique values (database restrictions).
     * @throws DBException              in all other cases.
     */
    void createFeedback(int id) throws DuplicateEntityException, DBException;
}
