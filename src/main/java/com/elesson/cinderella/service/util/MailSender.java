package com.elesson.cinderella.service.util;

import com.sun.mail.smtp.SMTPMessage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Properties;
import java.util.ResourceBundle;

/**
 * The {@code MailSender} class serves to prepare a message using
 * configuration files and send a message to client.
 */
public class MailSender {

    private static final Logger logger = LogManager.getLogger(MailSender.class);
    private static ResourceBundle appRB = ResourceBundle.getBundle("cinderella");
    private static Properties props = new Properties();
    private static String mailUser;
    private static String mailPassword;
    private static String mailSubject;
    private static String mailBody;

    static {
        mailSubject = appRB.getString("mail.subject");
        mailBody = appRB.getString("mail.body");
        try (InputStream is = new FileInputStream(appRB.getString("mail.server.props"))) {
            props.load(is);
            mailUser = props.getProperty("mail.user");
            mailPassword = props.getProperty("mail.password");
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    /**
     * Send a message to client specified by email address.
     *
     * @param address the address to which to send the message
     * @param date    the date of client's visit to be inserted in message body
     * @param time    the time of client's seance  to be inserted in message body
     * @throws MessagingException the messaging exception
     */
    public static synchronized void send(String address, LocalDate date, LocalTime time) throws MessagingException {

        Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(mailUser, mailPassword);
            }
        });

        String text = mailBody.replace("#date", date.toString()).replace("#time", time.toString());

        SMTPMessage message = new SMTPMessage(session);
        message.setFrom(new InternetAddress(mailUser));
        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(address));
        message.setSubject(mailSubject);
        message.setText(text);
        message.setNotifyOptions(SMTPMessage.NOTIFY_SUCCESS);
        Transport.send(message);
    }
}
