<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.language!=null ? sessionScope.language : pageContext.request.locale}"/>
<fmt:setBundle basename="messages.app"/>
<html>
<jsp:include page="../fragments/headTag.jsp"/>
<body>
<c:if test="${message == 'db'}">
    <div class="error"><fmt:message key="errmsg.db"/></div>
</c:if>

<c:if test="${message == 'access'}">
    <div class="error"><fmt:message key="errmsg.access"/></div>
</c:if>

<c:if test="${message == 'minvalid'}">
    <div class="error"><fmt:message key="errmsg.minvalid"/></div>
</c:if>

<c:if test="${message == 'duplicate'}">
    <div class="error"><fmt:message key="errmsg.duplicate"/></div>
</c:if>

<c:if test="${message == 'notFound'}">
    <div class="error"><fmt:message key="errmsg.notfound"/></div>
</c:if>

</body>
</html>
