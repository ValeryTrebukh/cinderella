<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.language!=null ? sessionScope.language : pageContext.request.locale}"/>
<fmt:setBundle basename="messages.app"/>
<html>
<jsp:include page="../fragments/headTag.jsp"/>
<body>
    <h2><fmt:message key="sch.write"/></h2>
<hr>
<div id="userForm">
    <form method="post" action="apply">
        <input type="hidden" name="feedbackId" value="${feedbackId}">
        <div><textarea name="feedback" cols="42" rows="10" required></textarea></div> <br>
        <button type="submit"><fmt:message key="user.save"/></button>
        <button onclick="window.history.back()" type="button"><fmt:message key="user.cancel"/></button>
    </form>
</div>
</body>
</html>
