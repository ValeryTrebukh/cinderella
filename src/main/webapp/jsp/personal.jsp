<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.language!=null ? sessionScope.language : pageContext.request.locale}"/>
<fmt:setBundle basename="messages.app"/>
<html>
    <jsp:include page="../fragments/headTag.jsp"/>
<body>
    <jsp:include page="../fragments/navMenu.jsp"/>
<br>
    <div id="shownPage">
        <div>
            <c:if test="${page > 1}"><a href="schedule?action=my&page=${page > 1 ? page - 1 : 1}">< < <</a></c:if>
            <c:if test="${page <= 1}">< < <</c:if>
        </div>
        <div>Page&nbsp;${page}&nbsp;of&nbsp;${pagesCount}</div>
        <div>
            <c:if test="${page < pagesCount}"><a href="schedule?action=my&page=${page < pagesCount ? page + 1 : pagesCount}">> > ></a></c:if>
            <c:if test="${page >= pagesCount}">> > ></c:if>
        </div>
    </div>
    <table>
        <thead>
        <tr>
            <th><fmt:message key="sch.master"/></th>
            <th><fmt:message key="sch.client"/></th>
            <th><fmt:message key="sch.date"/></th>
            <th><fmt:message key="sch.time"/></th>
            <th></th>
        </tr>
        </thead>
        <c:forEach items="${apps}" var="app">
            <jsp:useBean id="app" type="com.elesson.cinderella.model.Appointment"/>
            <tr>
                <td>${app.master.name}</td>
                <td>${app.client.name}</td>
                <td>${app.date}</td>
                <td>${app.seance.toString()}</td>
                <td><a href="apply?action=cancel&id=${app.id}"><fmt:message key="sch.cancel"/></a></td>
            </tr>
        </c:forEach>
    </table>
</body>
</html>
